const got = require('./data.js');

let houses = [];
let people = [];

(function (got){
    houses = got.houses;
    for(let house of houses){
        for(let ppl of house.people){
            ppl.houseName = house.name;
            people.push(ppl);
        }
    }
})(got)


// 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.
 const countAllPeople = (people) => {
   return people.length;
 };

console.log(`total number of people are :`, countAllPeople(people));

//////////////////2////////////////

const peopleByHouses = (houses, people) => {
    const res = {};
    for(let house of houses){
        res[house.name] = house.people.length;
    }
    return res;
};

console.log(`number of people by houses are :`, peopleByHouses(houses));


//////////////////3////////

const everyone = (people) =>{
    const names = [];
    for(let ppl of people){
        names.push(ppl.name)
    }
    return names;
}

console.log(`names are:`, everyone(people));

////////////////////4////////////////

const nameWithS = (helperFunction, people) =>{
    let names = helperFunction(people);
    const result = [];
    for(let name of names){
        const chars = name.split('');
        if(chars.includes('s') || chars.includes('S')){
            result.push(name);
        }
    }
    return result;
}

console.log(`names with s or S are :`, nameWithS(everyone, people));

///////////////// 5///////////////////////////


const nameWithA = (helperFunction, people) => {
  let names = helperFunction(people);
  const result = [];
  for (let name of names) {
    const chars = name.split("");
    if (chars.includes("A") || chars.includes("a")) {
      result.push(name);
    }
  }
  return result;
};

console.log(`names with a or A are :`, nameWithA(everyone, people));


///////////////6////////////////////

const surnameWithS = (helperFunction, people) => {
  let names = helperFunction(people);
  const result = [];
  for (let name of names) {
    const chars = name.split(' ')[1].split("");
    if (chars.includes("s") || chars.includes("S")) {
      result.push(name);
    }
  }
  return result;
};

console.log(`surnames with s or S are :`, surnameWithS(everyone, people));

/////////////////////////7/////////////////////

const surnameWithA = (helperFunction, people) => {
  let names = helperFunction(people);
  const result = [];
  for (let name of names) {
    const chars = name.split(" ")[1].split("");
    if (chars.includes("a") || chars.includes("A")) {
      result.push(name);
    }
  }
  return result;
};

console.log(`surnames with a or A are :`, surnameWithA(everyone, people));


//////////////8//////////////////////////


const peopleNameOfAllHouses = (people) =>{
    const result = {};
    for(let ppl of people){
        if(result[ppl.houseName]){
            result[ppl.houseName].push(ppl.name);
        }else{
            result[ppl.houseName] = [ppl.name];
        }
    }
    return result;
}

console.log(peopleNameOfAllHouses(people));